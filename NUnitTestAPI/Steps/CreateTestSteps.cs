﻿using Newtonsoft.Json;
using NUnit.Framework;
using NUnitTestAPI.Controllers;
using NUnitTestAPI.DTO;
using RestSharp;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using FluentAssertions;
using TechTalk.SpecFlow.Assist;

namespace NUnitTestAPI.Steps
{
    [Binding]
    public sealed class CreateTestSteps : CreateController
    {
        private EmployeeDataDTO actualEmployeeDataDTO;
        private EmployeeDTO employeeDto;
        private IRestResponse response;
        private WrongDataDTO wrongDataDTO;
        private WrongDTO wrongDTO;


        [Given(@"Given the right request for the employee creation is prepared with the following parameters")]
        public void GivenGivenTheRightRequestForTheEmployeeCreationIsPreparedWithTheFollowingParameters(Table table)
        {
            this.actualEmployeeDataDTO = table.CreateInstance<EmployeeDataDTO>();
        }

        [Given(@"Given the wrong request for the employee creation is prepared with the following parameters")]
        public void GivenGivenTheWrongRequestForTheEmployeeCreationIsPreparedWithTheFollowingParameters(Table table)
        {
            this.wrongDataDTO = table.CreateInstance<WrongDataDTO>();
        }

        [When(@"the user send the post request with the right data to create the new record in the database")]
        public async Task WhenISendThePostRequestToCreateTheNewRecordInTheDatabase()
        {
            this.response = await this.PostCreateEmployeeAsync(actualEmployeeDataDTO);
        }

        [When(@"the user send the post request with the wrong data to create the new record in the database")]
        public async Task WhenISendThePostRequestWithTheWrongDataToCreateTheNewRecordInTheDatabase()
        {
            this.response = await this.PostCreateEmployeeAsync(wrongDataDTO);
        }

        [Then(@"The response has been success")]
        public void ThenTheResponseHasBeenSuccess()
        {
            this.employeeDto = JsonConvert.DeserializeObject<EmployeeDTO>(response.Content);
            this.employeeDto.data.salary.Should().Be(actualEmployeeDataDTO.salary);
        }

        [Then(@"The reponse has been failed")]
        public void ThenTheReponseHasBeenFailed()
        {
            this.wrongDTO = JsonConvert.DeserializeObject<WrongDTO>(response.Content);
            this.wrongDTO.status.Should().Be("success");
        }
    }
}