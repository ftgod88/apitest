﻿Feature: Create

#Positive test the create the new person in the database
@#1 @positive
Scenario: When the user send the request with right data, then the response status has been success
	Given Given the right request for the employee creation is prepared with the following parameters
		| name | salary | age |
		| test | 123    | 23  |
	When the user send the post request with the right data to create the new record in the database
	Then The response has been success

#Закидываю wrongDTO Negative test the creat the new person in the databae with wrong data
@#1 @negative
Scenario: When the user send the request with wrong data, then the response status has been success
	Given Given the wrong request for the employee creation is prepared with the following parameters
		| lastName | conuter | itemName |
		| j        | true    | a        |
	When the user send the post request with the wrong data to create the new record in the database
	Then The reponse has been failed