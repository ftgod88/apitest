﻿using System.Runtime.Serialization;

namespace NUnitTestAPI.DTO
{
    
    public class EmployeeDTO
    {
        public EmployeeDTO()
        {
            data = new EmployeeDataDTO();
        }

        public string status { get; set; }

        public EmployeeDataDTO data { get; set; }  

        public string message { get; set; }
    }

    public class EmployeeDataDTO
    {
        public string name { get; set; }

        public int salary { get; set; }

        public int age { get; set; }

        public int id { get; set; }
    }
}
