﻿Feature: DeleteTest

#id 1 ꝏ
@1 @positive 
Scenario: When the user send delete request with right id, then the employee has been delete from the database
	Given The right "1" id for delete request has been entered
	When The user send delete request with right id 
	Then The employee has been deleted from the database

#id -ꝏ -1
@2 @negative
Scenario: When the user send delete request with wrong id, then the employee has been not deleted from the database
	Given The wrong "-1" id for delete request has been entered
	When The user send delete request with wrong id 
	Then The employee with the wrong id has been not deleted from the database

#id 0
@3 @negative
Scenario: When the user send delete request with zero instead of the id, then the employee has been not deleted from the database
	Given The wrong "0" id for delete request has been entered
	When The user send delete request with wrong id 
	Then The employee with the wrong id has been not deleted from the database

#id a
@4 @negative
Scenario: When the user send delete request with char instead of the id, then the employee has been not deleted from the database
	Given The wrong "a" id for delete request has been entered
	When The user send delete request with wrong id 
	Then The employee with the wrong id has been not deleted from the database
 
#id !
@5 @negative
 Scenario: When the user send delete request with symbol instead of the id, then the employee has been not deleted from the database
	Given The wrong "!" id for delete request has been entered
	When The user send delete request with wrong id 
	Then The employee with the wrong id has been not deleted from the database