﻿using System;
using TechTalk.SpecFlow;

namespace NUnitTestAPI.DTO
{
    public class WrongDTO
    {
        public WrongDTO()
        {
            data = new WrongDataDTO();
        }

        public string status { get; set; }

        public WrongDataDTO data { get; set; }

        public string message { get; set; }
    }

    public class WrongDataDTO
    {
        public string lastName { get; set; }

        public bool counter { get; set; }

        public string itemName { get; set; }

        public string id { get; set; }
    }
}