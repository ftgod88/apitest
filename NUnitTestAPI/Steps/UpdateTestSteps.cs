﻿using FluentAssertions;
using NUnitTestAPI.Controllers;
using NUnitTestAPI.DTO;
using RestSharp;
using System.Net;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace NUnitTestAPI.Steps
{
    [Binding]
    public sealed class UpdateTestSteps : UpdateConroller
    {
        private IRestResponse response;
        private string employeeId;
        private EmployeeDataDTO employeeDataDTO;

        [Given(@"The data info has been entered with the following parameters")]
        public void GivenTheDataInfoHasBeenEnteredWithTheFollowingParameters(Table table)
        {
            this.employeeDataDTO = table.CreateInstance<EmployeeDataDTO>();
        }

        [Given(@"The wrong ""([^""]*)"" id for put request has been entered")]
        [Given(@"The right ""([^""]*)"" id for put request has been entered")]
        public void GivenTheRightIdForPutRequestHasBeenEntered(string employeeId)
        {
            this.employeeId = employeeId;
        }

        [When(@"The user send put request with wrong id")]
        [When(@"The user send put request with right id")]
        public async Task WhenISendPutRequestWithRightId()
        {
            this.response = await PutUpdateEmployeeAsync(employeeDataDTO,employeeId);
        }

        [Then(@"The employee has been updated in the database")]
        public void ThenTheEmployeeHasBeenUpdatedInTheDatabase()
        {
            this.response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Then(@"The employee with the wrong id has been not updated in the database")]
        public void ThenTheEmployeeWithTheWrongIdHasBeenNotUpdatedInTheDatabase()
        {
            this.response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }
}