﻿using NUnitTestAPI.DTO;
using RestSharp;
using System.Threading.Tasks;

namespace NUnitTestAPI.Controllers
{
    public class UpdateConroller : BaseController
    {
        private const string PutUpdateEmployeeUrl = "update/{0}";

        public async Task<IRestResponse> PutUpdateEmployeeAsync(EmployeeDataDTO employeeDTO, string employeeId)
        {
            var resource = string.Join(this.BaseUrl, string.Format(PutUpdateEmployeeUrl, employeeId));
            return await this.PutAsync(resource, employeeDTO);
        }
    }
}
