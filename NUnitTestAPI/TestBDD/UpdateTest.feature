﻿Feature: UpdateTest
Background:
	Given The data info has been entered with the following parameters
		| Name | Salary | Age |
		| test | 123    | 23  |

Scenario: When the user send delete request with right id, then the employee has been updated in the database
	Given The right "1" id for put request has been entered
	When The user send put request with right id
	Then The employee has been updated in the database

Scenario: When the user send delete request with wrong id, then the employee has been updated in the database
	Given The wrong "-1" id for put request has been entered
	When The user send put request with wrong id
	Then The employee with the wrong id has been not updated in the database

Scenario: When the user send delete request with zero instead of the id, then the employee has been not updated in the database
	Given The wrong "0" id for put request has been entered
	When The user send put request with wrong id
	Then The employee with the wrong id has been not updated in the database

Scenario: When the user send delete request with char instead of the id, then the employee has been not updated in the database
	Given The wrong "a" id for put request has been entered
	When The user send put request with wrong id
	Then The employee with the wrong id has been not updated in the database

Scenario: When the user send delete request with symbol instead of the id, then the employee has been not updated in the database
	Given The wrong "!" id for put request has been entered
	When The user send put request with wrong id
	Then The employee with the wrong id has been not updated in the database