﻿using RestSharp;
using System.Threading.Tasks;

namespace NUnitTestAPI.Controllers
{
    public class CreateController : BaseController
    {
        private const string PostCreateEmployeeUrl = "/create";

        public async Task<IRestResponse> PostCreateEmployeeAsync(object actualEmployeeDataDTO)
        {
            var resource = string.Join(this.BaseUrl, PostCreateEmployeeUrl);
            return await this.PostAsync(resource, actualEmployeeDataDTO);
        }
    }
}
