﻿using NUnitTestAPI.ConfigFiles;
using NUnitTestAPI.DTO;
using RestSharp;
using System.Threading.Tasks;

namespace NUnitTestAPI.Controllers
{
    public class BaseController
    {
        protected string BaseUrl => Manager.ConfigurationUrl();
        protected RestClient RestClient => new RestClient(this.BaseUrl);

        protected async Task<IRestResponse> GetAsync(string resource)
        {
            var request = new RestRequest(resource, Method.GET);
            return await this.RestClient.ExecuteAsync<RestResponse>(request, Method.GET);
        }

        protected async Task<IRestResponse> PutAsync(string resource, EmployeeDataDTO employeeDTO)
        {
            var request = new RestRequest(resource, Method.PUT);
            request.AddJsonBody(employeeDTO);
            return await this.RestClient.ExecuteAsync<RestResponse>(request, Method.PUT);
        }

        protected async Task<IRestResponse> PostAsync(string resource, object employeeDTO)
        {
            var request = new RestRequest(resource, Method.POST);
            request.AddJsonBody(employeeDTO);
            return await this.RestClient.ExecuteAsync<RestResponse>(request, Method.POST);
        }

        protected async Task<IRestResponse> DeleteAsync(string resource)
        {
            var request = new RestRequest(resource, Method.DELETE);
            return await this.RestClient.ExecuteAsync<RestResponse>(request, Method.DELETE);
        }
    }
}
