﻿using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using NUnitTestAPI.Controllers;
using NUnitTestAPI.DTO;
using RestSharp;
using System.Net;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace NUnitTestAPI.Steps
{
    [Binding]
    public sealed class DeleteTestSteps : DeleteConroller
    {
        private IRestResponse response;
        private string employeeId;
        private EmployeeDTO employeeDTO;

        [Given(@"The right ""([^""]*)"" id for delete request has been entered")]
        [Given(@"The wrong ""([^""]*)"" id for delete request has been entered")]
        public void GivenTheWrongIdHasBeenEntered(string id)
        {
            this.employeeId = id;
        }

        [When(@"The user send delete request with wrong id")]
        [When(@"The user send delete request with right id")]
        public async Task WhenISendDeleteRequestWithRightId()
        {
            this.response = await DeleteEmployeeAsync(employeeId);
        }

        [Then(@"The employee has been deleted from the database")]
        public void ThenTheEmployeeHasBeenDeletedDromTheDatabase()
        {
            this.response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Then(@"The employee with the wrong id has been not deleted from the database")]
        public void ThenTheEmployeeWithTheWrongIdHasBeenNotDeletedFromTheDatabase()
        {
            this.response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}