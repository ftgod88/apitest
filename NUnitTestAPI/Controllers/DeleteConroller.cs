﻿using RestSharp;
using System.Threading.Tasks;

namespace NUnitTestAPI.Controllers
{
    public class DeleteConroller : BaseController
    {
        private const string DeleteEmployeeUrl = "/delete/{0}";

        public async Task<IRestResponse> DeleteEmployeeAsync(string employeeId)
        {
            var resource = string.Join(this.BaseUrl, string.Format(DeleteEmployeeUrl, employeeId));
            return await this.DeleteAsync(resource);
        }
    }
}
