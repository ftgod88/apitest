﻿Feature: Employee

@tag1
Scenario: When the user send get request to the employees, then the response have the all data about employes
	When The user send get request 
	Then The response has been cotained info about employees

Scenario: When the user send get request with right id, then the response have data about choose employee
	Given The right "1" id for get request has been entered
	When The user send get request with right id
	Then The response has been cotained info about employee

Scenario: When the user  send get request with wrong id, then the response status is Not Found
	Given The wrong "-1" id for get request has been entered
	When The user send get request with wrong id
	Then The response has been not cotained info about employee

Scenario: When the user  send get request with zero instead of the id, then the response status is Not Found
	Given The wrong "0" id for get request has been entered
	When The user send get request with wrong id
	Then The response has been not cotained info about employee

Scenario: When the user  send get request with char instead of the id, then the response status is Not Found
	Given The wrong "a" id for get request has been entered
	When The user send get request with wrong id
	Then The response has been not cotained info about employee

Scenario: When the user  send delete request with symbol instead of the id, then the response status is Not Found
	Given The wrong "!" id for get request has been entered
	When The user send get request with wrong id
	Then The response has been not cotained info about employee