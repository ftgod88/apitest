﻿using FluentAssertions;
using NUnit.Framework;
using NUnitTestAPI.Controllers;
using RestSharp;
using System.Net;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace NUnitTestAPI.ConfigFiles
{
    [Binding]
    public sealed class EmployeeTestSteps : EmployeeController
    {
        private IRestResponse response;
        private string employeeId;

        [Given(@"The wrong ""([^""]*)"" id for get request has been entered")]
        [Given(@"The right ""([^""]*)"" id for get request has been entered")]
        public void GivenTheRightIdForGetRequestHasBeenEntered(string employeeId)
        {
            this.employeeId = employeeId;
        }

        [When(@"The user send get request")]
        public async Task WhenISendGetRequest()
        {
            this.response = await GetEmployeeAsync();
        }

        [When(@"The user send get request with wrong id")]
        [When(@"The user send get request with right id")]
        public async Task WhenISendGetRequestWithRightId()
        {
            this.response = await GetEmployeeByIdAsync(employeeId);
        }

        [Then(@"The response has been cotained info about employees")]
        [Then(@"The response has been cotained info about employee")]
        public void ThenTheResponseHasBeenCotainedInfoAboutEmployee()
        {
            this.response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Then(@"The response has been not cotained info about employee")]
        public void ThenTheResponseHasBeenNotCotainedInfoAboutEmployee()
        {
            this.response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}